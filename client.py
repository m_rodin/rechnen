#! python3.10
import os,socket,hashlib
from random import randint

host=("127.0.0.1",50000) # ADDRESS FOR SERVER TO CONNECT TO
s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("0.0.0.0",randint(50000,50099)))

#-------------------------------------------------------------------DICTS WITH INFORMATION FOR THE USER
actions={
    "DUMP":"Zwischenstand einsehen",
    "START":"Startet eine Operation",
    "VAL":"Fügt zur Operation einen Wert hinzu",
    "END":"Beendet eine Operation und gibt das Ergebnis aus",
    "STARTSESSION":"Startet eine neue Sitzung",
    "ENDSESSION":"Beendet die Sitzung",
    "EXIT":"Verlasse das Skript"
}
valtypes= {
    "ADD":"Addition",
    "SUB":"Subtraktion",
    "MUL":"Multiplikation",
    "DIV":"Division"
}

#-------------------------------------------------------------------CHECK FOR EXISTING AND VALID SESSION KEY
session=open("session.txt","r").read()
bsess=False
if not session:
    bsess=True
    tmp=input("No session cookie found. Get a new one? (Y/n)")
    tmp+=" "
    if tmp== "n" or tmp=="N":
        bsess=False
else:
    s.sendto(f"{session}:DUMP:".encode(),host)
    data=s.recvfrom(1024)[0].decode()
    if data.startswith("ERROR:"):
        bsess=True
        tmp=input("Session cookie timed out. Get a new one? (Y/n)")
        if tmp== "n" or tmp=="N":
            bsess=False
    else:
        print("Jetziger Stand: ",":".join(data.split(":")[2:]))

#-------------------------------------------------------------------IF NO VALID SESSION KEY, GET A NEW ONE
if bsess:
    sfile=open("session.txt","wb")
    s.sendto(b':STARTSESSION:',host)
    message,address=s.recvfrom(1024)
    session=message.decode().split(":")[-1].encode()
    sfile.write(session)
    session=session.decode()
    sfile.close()

#-------------------------------------------------------------------MAIN LOOP
def main(session):
    bexit=False
    while not bexit:
        action=""
        val=""
        # Print out all possible commands
        for act in actions:
            print(act,":",actions[act])
        ergebnis=input("Wähle eine Aktion aus: ")
        if ergebnis.lower() in "exit":
            s.close()
            exit()
        # Check if entered command is allowed
        if not ergebnis.upper() in actions:
            input("Dieser Befehl wird nicht unterstützt!")
            continue
        for act in actions:
            if ergebnis.lower() in act.lower():
                action=act
                break
        # If not in list, ask for additional value
        if not action in ["DUMP","STARTSESSION","END","ENDSESSION"]:
            if action=="START":
                for valtype in valtypes:
                    print(valtype,":",valtypes[valtype])
            val=input("Füge einen Wert hinzu: ")
            for valtype in valtypes:
                if val.lower() in valtype.lower():
                    val=valtype
                    break
        # Send command to server
        message=f"{session}:{action}:{val}"
        messagehash=hashlib.md5(message.encode()).hexdigest()
        s.sendto(message.encode(), host)

        res=s.recvfrom(1024)[0].decode()
        reslist=res.split(":")
        # Check if hash is the same
        if reslist[1] != messagehash:
            print("Der Hash der Nachricht ist falsch!")
        # Check if server returned an error
        rescode="Ergebnis"
        if reslist[0] == "ERROR":
            rescode="Fehler"

        print(f"{rescode}: {':'.join(reslist[2:])}")
        if action=="ENDSESSION":
            session=""
        if action == "STARTSESSION":
            if not res.startswith("ERROR"):
                session=reslist[-1]
                sfile=open("session.txt","w")
                sfile.write(session)
                sfile.close()

        input("Enter...")
    

if __name__ == "__main__":
    try:
        main(session)
    except KeyboardInterrupt:
        s.close()