#! python3.10
import socket,ast,hashlib
import random

#-------------------------------------------------------------------FUNCTIONS
def parserequest(req,address): # Parse request from client
    rdict={}
    reqlist=req.split(":")
    if not req.count(":") == 2:
        code=1
        s.sendto(f"ERROR:Invalid format:{req}".encode(), address)
        return ["ERROR","",""]
    return reqlist

#-------------------------------------------------------------------
commands={"ADD":"+","MUL":"*","SUB":"-","DIV":"/"}
sessions={} # "SESSION":{"action":"ADD","values":[],"address":""}

s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("0.0.0.0",50000))

#-------------------------------------------------------------------MAIN LOOP
def main():
    while True:
        # Get data from client and parse the request
        code=0
        message, address = s.recvfrom(1024)
        messagehash=hashlib.md5(message).hexdigest()
        session, action, val = parserequest(message.decode(),address)
        # Reset if request is invalid
        if not action:
            continue
        print("["+address[0]+"]",message.decode(), end=' ') # Print first part of log
        if action=="DUMP":
            if session in sessions:
                values=f"[{', '.join(sessions[session]['values'])}]"
                res=f"{session}:{sessions[session]['action']}:{values}"
            else:
                code=1
                res="Invalid session key"
        elif action=="STARTSESSION":
            if session in sessions:
                code=1
                res="Session already exists"
            else:
                session=""
                while session in sessions or not session: # creates random session key
                    session="".join(random.choice("0123456789ABCDEF") for i in range(6))
                sessions[session]={"action":"","values":[],"address":address}
                res=session
        elif not session in sessions:
            code=1
            res="Invalid session key"
        elif action=="ENDSESSION":
            sessions.pop(session) # removes session from dict
            res=session
        elif action=="START":
            if not sessions[session]["action"]:
                if val in commands:
                    sessions[session]["action"]=val
                    res=val
                else:
                    code=1
                    res="Invalid option"
            else:
                code=1
                res="Operation already set"
        elif action=="VAL":
            if "[" in val and "]" in val: # parse list of values
                vallist=ast.literal_eval(val)
                for i in vallist:
                    sessions[session]["values"].append(str(i))
            else: # parse single value
                sessions[session]["values"].append(val)
            res=str(val)
        elif action=="END":
            if not sessions[session]["action"]:
                code=1
                res="no operation started"
            else:
                # calculates the final value with operator values
                vallist=sessions[session]["values"]
                operator=commands[sessions[session]["action"]]
                if len(vallist) > 1:
                    res=str(eval(operator.join(vallist)))
                else:
                    code=1
                    res="Operation can't be finished"
                sessions[session]={"action":"","values":[]} # resets the session to blank state
        else:
            code=1
            res="Invalid action"

        print("→",res) # Print second part of log
        # Error if code==1, else ok
        if code:
            res=f"ERROR:{messagehash}:{res}"
        else:
            res=f"OK:{messagehash}:{res}"
        s.sendto(res.encode(), address) # send response

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        s.close()